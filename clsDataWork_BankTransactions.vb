﻿Imports Ontology_Module
Imports Filesystem_Module
Imports OntologyAppDBConnector
Imports OntologyClasses.BaseClasses
Public Class clsDataWork_BankTransactions

    Private dtblT_Banktransactions As DataSet_Transactions.dtbl_BanktransactionsDataTable

    Private objDBLevel_ImportSettings_Att_ColHeader As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Att_Start As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_Files As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_TextQualifier As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_TextSeperator As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_ImportLog As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_IS_To_BankClass As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_Bank As OntologyModDBConnector
    Private objDBLevel_ImportSettings_Rel_Partner As OntologyModDBConnector
    Private objDBLevel_Kontodaten As OntologyModDBConnector

    Private objDBLevel_BTR As OntologyModDBConnector
    Private objDBLevel_BTR_Info As OntologyModDBConnector
    Private objDBLevel_BTR_Valuta As OntologyModDBConnector
    Private objDBLevel_BTR_Zahlungsausgang As OntologyModDBConnector
    Private objDBLevel_BTR_BegZahl As OntologyModDBConnector
    Private objDBLevel_BTR_Betrag As OntologyModDBConnector
    Private objDBLevel_BTR_Buchungstext As OntologyModDBConnector
    Private objDBLevel_BTR_Verwendungszweck As OntologyModDBConnector
    Private objDBLevel_BTR_Currencies As OntologyModDBConnector
    Private objDBLevel_BTR_AltCurr As OntologyModDBConnector
    Private objDBLevel_BTR_Payment As OntologyModDBConnector
    Private objDBLevel_BTR_Auftragskonto As OntologyModDBConnector
    Private objDBLevel_BTR_BLZ As OntologyModDBConnector
    Private objDBLevel_BTR_Gegenkonto As OntologyModDBConnector

    Private objThread_BTR As Threading.Thread
    Private objThread_BTR_Info As Threading.Thread
    Private objThread_BTR_Valuta As Threading.Thread
    Private objThread_BTR_Zahlungsausgang As Threading.Thread
    Private objThread_BTR_BegZahl As Threading.Thread
    Private objThread_BTR_Betrag As Threading.Thread
    Private objThread_BTR_Buchungstext As Threading.Thread
    Private objThread_BTR_Verwendungszweck As Threading.Thread
    Private objThread_BTR_Currencies As Threading.Thread
    Private objThread_BTR_AltCurr As Threading.Thread
    Private objThread_BTR_Payment As Threading.Thread
    Private objThread_BTR_Auftragskonto As Threading.Thread
    Private objThread_BTR_BLZ As Threading.Thread
    Private objThread_BTR_Gegenkonto As Threading.Thread
    Private objThread_Data As Threading.Thread

    Private objFileWork As clsFileWork

    Private objOItem_BankTransactionClass As clsOntologyItem

    Private objOItem_Partner As clsOntologyItem

    Private objOItem_ImportSetting As clsOntologyItem

    Private objLocalConfig As clsLocalConfig

    Private objOItem_Result_BankTransaction As clsOntologyItem

    Private objOItem_Konto_Mandant As clsOntologyItem

    Public ReadOnly Property OItem_Konto_Mandant As clsOntologyItem
        Get
            Return objOItem_Konto_Mandant
        End Get
    End Property

    Public ReadOnly Property OItem_ImportSetting As clsOntologyItem
        Get
            Return objOItem_ImportSetting
        End Get
    End Property

    Public ReadOnly Property OItem_Result_BankTransactions As clsOntologyItem
        Get
            Return objOItem_Result_BankTransaction
        End Get
    End Property

    Public ReadOnly Property dtbl_Transactions As DataSet_Transactions.dtbl_BanktransactionsDataTable
        Get
            Return dtblT_Banktransactions
        End Get

    End Property

    Public ReadOnly Property FirstColHeader As Boolean
        Get
            Dim boolFirstColHeader As Boolean


            Dim objLHeader = From obj In objDBLevel_ImportSettings_Att_ColHeader.ObjAtts
                             Where obj.ID_Object = objOItem_ImportSetting.GUID
                             Where obj.ID_AttributeType = objLocalConfig.OItem_Attribute_First_Line_Col_Header.GUID
                             Select obj.Val_Bit

            If objLHeader.Count > 0 Then
                boolFirstColHeader = objLHeader(0)
            Else
                boolFirstColHeader = False
            End If

            Return boolFirstColHeader
        End Get
    End Property

    Public ReadOnly Property LastImport As Date
        Get
            Dim dateLastImport As Date


            Dim objLLastImport = From objImport In objDBLevel_ImportSettings_Rel_ImportLog.ObjectRels
                                 Where objImport.ID_Other = objOItem_ImportSetting.GUID
                                 Where objImport.ID_Parent_Object = objLocalConfig.OItem_Type_Imports.GUID
                                 Join objStart In objDBLevel_ImportSettings_Att_ColHeader.ObjAtts On objImport.ID_Object Equals objStart.ID_Object
                                 Where objStart.ID_AttributeType = objLocalConfig.OItem_Attribute_Start.GUID
                                 Order By objStart.Val_Date Descending



            If objLLastImport.Count > 0 Then
                dateLastImport = objLLastImport(0).objStart.Val_Date
            Else
                dateLastImport = Nothing
            End If

            Return dateLastImport
        End Get
    End Property

    Public ReadOnly Property OItem_File As clsOntologyItem
        Get
            Dim objOItem_File As New clsOntologyItem



            Dim objLFile = From obj In objDBLevel_ImportSettings_Rel_Files.ObjectRels Where obj.ID_Object = objOItem_ImportSetting.GUID
                           Where obj.ID_Parent_Other = objLocalConfig.OItem_Type_File.GUID
                           Order By obj.OrderID
                           Select ID_Other = obj.ID_Other, Name_Other = obj.Name_Other, ID_Parent_Other = obj.ID_Parent_Other

            If objLFile.Count > 0 Then
                objOItem_File.GUID = objLFile(0).ID_Other
                objOItem_File.Name = objLFile(0).Name_Other
                objOItem_File.GUID_Parent = objLFile(0).ID_Parent_Other
                objOItem_File.Type = objLocalConfig.Globals.Type_Object

                objOItem_File.Additional1 = objFileWork.get_Path_FileSystemObject(objOItem_File, False)

            Else
                objOItem_File = Nothing
            End If

            Return objOItem_File
        End Get
    End Property

    Public ReadOnly Property OItem_TextQualifier As clsOntologyItem
        Get
            Dim objOItem_TextQualifier As clsOntologyItem

            Dim objLQualifier = From obj In objDBLevel_ImportSettings_Rel_TextQualifier.ObjectRels
                                Where obj.ID_Object = objOItem_ImportSetting.GUID
                                Select ID_Other = obj.ID_Other, Name_Other = obj.Name_Other, ID_Parent_Other = obj.ID_Parent_Other

            If objLQualifier.Count > 0 Then
                objOItem_TextQualifier = New clsOntologyItem(objLQualifier(0).ID_Other, _
                                                             objLQualifier(0).Name_Other, _
                                                             objLQualifier(0).ID_Parent_Other, _
                                                             objLocalConfig.Globals.Type_Object)


            Else
                objOItem_TextQualifier = Nothing
            End If

            Return objOItem_TextQualifier
        End Get
    End Property

    Public ReadOnly Property OItem_TextSeperator As clsOntologyItem
        Get
            Dim objOItem_TextSeperator As clsOntologyItem

            Dim objLSeperator = From obj In objDBLevel_ImportSettings_Rel_TextSeperator.ObjectRels
                                Where obj.ID_Object = objOItem_ImportSetting.GUID
                                Select ID_Other = obj.ID_Other, Name_Other = obj.Name_Other, ID_Parent_Other = obj.ID_Parent_Other

            If objLSeperator.Count > 0 Then
                objOItem_TextSeperator = New clsOntologyItem(objLSeperator(0).ID_Other, _
                                                             objLSeperator(0).Name_Other, _
                                                             objLSeperator(0).ID_Parent_Other, _
                                                             objLocalConfig.Globals.Type_Object)


            Else
                objOItem_TextSeperator = Nothing
            End If

            Return objOItem_TextSeperator
        End Get
    End Property

    Public ReadOnly Property OItem_Class_Banktransactions As clsOntologyItem
        Get
            Dim objOItem_Class_Banktransactions As clsOntologyItem

            Dim objLTransactions = From obj In objDBLevel_ImportSettings_Rel_IS_To_BankClass.ObjectRels
                                   Where obj.ID_Object = objOItem_ImportSetting.GUID
                                   Select ID_Class = obj.ID_Other, Name_Class = obj.Name_Other, ID_Parent_Other = obj.ID_Parent_Other

            If objLTransactions.Count > 0 Then
                objOItem_Class_Banktransactions = New clsOntologyItem(objLTransactions(0).ID_Class, _
                                                                      objLTransactions(0).Name_Class, _
                                                                      objLTransactions(0).ID_Parent_Other, _
                                                                      objLocalConfig.Globals.Type_Class)
            Else
                objOItem_Class_Banktransactions = Nothing
            End If

            Return objOItem_Class_Banktransactions
        End Get
    End Property

    Public Function get_ImportSettings() As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim objOL_ImportSettings_Rel As New List(Of clsObjectRel)
        Dim objOL_ImportSettings_Att As New List(Of clsObjectAtt)

        objOL_ImportSettings_Att.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                            .ID_AttributeType = objLocalConfig.OItem_Attribute_First_Line_Col_Header.GUID})

        objOL_ImportSettings_Att.Add(New clsObjectAtt With {.ID_Class = objLocalConfig.OItem_Type_Imports.GUID,
                                                            .ID_AttributeType = objLocalConfig.OItem_Attribute_Start.GUID})

        objOItem_Result = objDBLevel_ImportSettings_Att_ColHeader.GetDataObjectAtt(objOL_ImportSettings_Att, _
                                                                           doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                .ID_Parent_Other = objLocalConfig.OItem_Type_File.GUID,
                                                                .ID_RelationType = objLocalConfig.OItem_RelationType_imports_from.GUID})

            objOItem_Result = objDBLevel_ImportSettings_Rel_Files.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                     doIds:=False)

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                objOL_ImportSettings_Rel.Clear()
                objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                    .ID_Parent_Other = objLocalConfig.OItem_Type_Text_Qualifier.GUID,
                                                                    .ID_RelationType = objLocalConfig.OItem_RelationType_works_with.GUID})

                objOItem_Result = objDBLevel_ImportSettings_Rel_TextQualifier.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                             doIds:=False)

                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    objOL_ImportSettings_Rel.Clear()

                    objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                        .ID_Parent_Other = objLocalConfig.OItem_Type_Text_Seperators.GUID,
                                                                        .ID_RelationType = objLocalConfig.OItem_RelationType_works_with.GUID})

                    objOItem_Result = objDBLevel_ImportSettings_Rel_TextSeperator.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                                     doIds:=False)

                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                        objOL_ImportSettings_Rel.Clear()

                        objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Imports.GUID,
                                                                            .ID_Parent_Other = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                            .ID_RelationType = objLocalConfig.OItem_RelationType_Log_of.GUID})

                        objOItem_Result = objDBLevel_ImportSettings_Rel_ImportLog.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                                     doIds:=False)

                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            objOL_ImportSettings_Rel.Clear()

                            objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                                .ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Banks.GUID})
                                
                            objOItem_Result = objDBLevel_ImportSettings_Rel_IS_To_BankClass.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                                         doIds:=False)

                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                objOL_ImportSettings_Rel.Clear()

                                objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                                    .ID_Parent_Other = objLocalConfig.OItem_Type_Bankleitzahl.GUID,
                                                                                    .ID_RelationType = objLocalConfig.OItem_RelationType_belonging.GUID})

                                objOItem_Result = objDBLevel_ImportSettings_Rel_Bank.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                                                         doIds:=False)

                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                    objOL_ImportSettings_Rel.Clear()

                                    objOL_ImportSettings_Rel.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Import_Settings.GUID,
                                                                                        .ID_Other = objOItem_Partner.GUID,
                                                                                        .ID_RelationType = objLocalConfig.OItem_RelationType_zugeh_rige_Mandanten.GUID})
                                        
                                    objOItem_Result = objDBLevel_ImportSettings_Rel_Partner.GetDataObjectRel(objOL_ImportSettings_Rel, _
                                                                           doIds:=False)

                                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then

                                        If objDBLevel_ImportSettings_Rel_Partner.ObjectRels.Count > 0 Then
                                            objOItem_ImportSetting = New clsOntologyItem(objDBLevel_ImportSettings_Rel_Partner.ObjectRels(0).ID_Object, _
                                                                                         objDBLevel_ImportSettings_Rel_Partner.ObjectRels(0).Name_Object, _
                                                                                        objLocalConfig.OItem_Type_Import_Settings.GUID, _
                                                                                        objLocalConfig.Globals.Type_Object)
                                            objOItem_Result = get_Data_Konto()
                                        Else
                                            objOItem_Result = objLocalConfig.Globals.LState_Error
                                        End If


                                    End If

                                End If
                            End If
                        End If
                    End If
                End If
            End If

        End If


        Return objOItem_Result
    End Function

    Public Function get_Data_Konto() As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem

        Dim objOList_Kontodaten_To_Partner As New List(Of clsObjectRel)

        objOList_Kontodaten_To_Partner.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Kontodaten.GUID,
                                                                  .ID_Other = objOItem_Partner.GUID,
                                                                  .ID_RelationType = objLocalConfig.OItem_RelationType_belongsTo.GUID})
            
        objOItem_Result = objDBLevel_Kontodaten.GetDataObjectRel(objOList_Kontodaten_To_Partner, doIds:=True)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOList_Kontodaten_To_Partner.Clear()
            If objDBLevel_Kontodaten.ObjectRelsId.Count > 0 Then
                objOList_Kontodaten_To_Partner.Add(New clsObjectRel With {.ID_Object = objDBLevel_Kontodaten.ObjectRelsId(0).ID_Object,
                                                                          .ID_Parent_Other = objLocalConfig.OItem_Type_Kontonummer.GUID,
                                                                          .ID_RelationType = objLocalConfig.OItem_RelationType_provides.GUID})
                    
                objOItem_Result = objDBLevel_Kontodaten.GetDataObjectRel(objOList_Kontodaten_To_Partner, _
                                                                           doIds:=False)

                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    If objDBLevel_Kontodaten.ObjectRels.Count > 0 Then
                        objOItem_Konto_Mandant = New clsOntologyItem(objDBLevel_Kontodaten.ObjectRels(0).ID_Other, _
                                                                     objDBLevel_Kontodaten.ObjectRels(0).Name_Other, _
                                                                     objLocalConfig.OItem_Type_Kontonummer.GUID, _
                                                                     objLocalConfig.Globals.Type_Object)
                    Else
                        objOItem_Result = objLocalConfig.Globals.LState_Error
                    End If
                End If
            Else
                objOItem_Result = objLocalConfig.Globals.LState_Error
            End If
        End If

        Return objOItem_Result
    End Function

    Public Function get_Data_BankTransaction(ByVal OItem_BankTransactionsClass As clsOntologyItem, ByVal dtblT_Banktransactions As DataSet_Transactions.dtbl_BanktransactionsDataTable) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem

        Me.dtblT_Banktransactions = dtblT_Banktransactions
        objOItem_Result = objLocalConfig.Globals.LState_Success


        Try
            objThread_BTR.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_AltCurr.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Auftragskonto.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_BegZahl.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Betrag.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_BLZ.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Buchungstext.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Currencies.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Gegenkonto.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Info.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Payment.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Valuta.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Verwendungszweck.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_BTR_Zahlungsausgang.Abort()
        Catch ex As Exception

        End Try

        Try
            objThread_Data.Abort()
        Catch ex As Exception

        End Try


        objOItem_BankTransactionClass = OItem_BankTransactionsClass

        objOItem_Result_BankTransaction = objLocalConfig.Globals.LState_Nothing

        objThread_BTR = New Threading.Thread(AddressOf get_Data_BTR)
        objThread_BTR_AltCurr = New Threading.Thread(AddressOf get_Data_AltCurr)
        objThread_BTR_Auftragskonto = New Threading.Thread(AddressOf get_Data_Auftragskonto)
        objThread_BTR_BegZahl = New Threading.Thread(AddressOf get_Data_BegZahl)
        objThread_BTR_Betrag = New Threading.Thread(AddressOf get_Data_Betrag)
        objThread_BTR_BLZ = New Threading.Thread(AddressOf get_Data_BLZ)
        objThread_BTR_Buchungstext = New Threading.Thread(AddressOf get_Data_Buchungstext)
        objThread_BTR_Currencies = New Threading.Thread(AddressOf get_Data_Currencies)
        objThread_BTR_Gegenkonto = New Threading.Thread(AddressOf get_Data_Gegenkonto)
        objThread_BTR_Info = New Threading.Thread(AddressOf get_Data_Info)
        objThread_BTR_Payment = New Threading.Thread(AddressOf get_Data_Payment)
        objThread_BTR_Valuta = New Threading.Thread(AddressOf get_Data_Valuta)
        objThread_BTR_Verwendungszweck = New Threading.Thread(AddressOf get_Data_Verwendungszweck)
        objThread_BTR_Zahlungsausgang = New Threading.Thread(AddressOf get_Data_Zahlungsausgang)
        objThread_Data = New Threading.Thread(AddressOf fill_DataTable)

        objThread_BTR.Start()
        objThread_BTR_AltCurr.Start()
        objThread_BTR_Auftragskonto.Start()
        objThread_BTR_BegZahl.Start()
        objThread_BTR_Betrag.Start()
        objThread_BTR_BLZ.Start()
        objThread_BTR_Buchungstext.Start()
        objThread_BTR_Currencies.Start()
        objThread_BTR_Gegenkonto.Start()
        objThread_BTR_Info.Start()
        objThread_BTR_Payment.Start()
        objThread_BTR_Valuta.Start()
        objThread_BTR_Verwendungszweck.Start()
        objThread_BTR_Zahlungsausgang.Start()
        objThread_Data.Start()


        Return objOItem_Result
    End Function

    Private Sub get_Data_BTR()
        Dim objOL_BTR As New List(Of clsOntologyItem)
        Dim objOItem_Result As clsOntologyItem

        objOL_BTR.Add(New clsOntologyItem(Nothing, _
                                          Nothing, _
                                          objOItem_BankTransactionClass.GUID, _
                                          objLocalConfig.Globals.Type_Object))

        objOItem_Result = objDBLevel_BTR.GetDataObjects(objOL_BTR)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Private Sub fill_DataTable()
        While objThread_BTR.IsAlive Or _
            objThread_BTR_AltCurr.IsAlive Or _
            objThread_BTR_Auftragskonto.IsAlive Or _
            objThread_BTR_BegZahl.IsAlive Or _
            objThread_BTR_Betrag.IsAlive Or _
            objThread_BTR_BLZ.IsAlive Or _
            objThread_BTR_Buchungstext.IsAlive Or _
            objThread_BTR_Currencies.IsAlive Or _
            objThread_BTR_Gegenkonto.IsAlive Or _
            objThread_BTR_Info.IsAlive Or _
            objThread_BTR_Payment.IsAlive Or _
            objThread_BTR_Valuta.IsAlive Or _
            objThread_BTR_Verwendungszweck.IsAlive Or _
            objThread_BTR_Zahlungsausgang.IsAlive

        End While

        If objOItem_Result_BankTransaction.GUID = objLocalConfig.Globals.LState_Nothing.GUID Then
            Dim objLTransactions = From objBTR In objDBLevel_BTR.Objects1
                                   Join objInfo In objDBLevel_BTR_Info.ObjAtts On objBTR.GUID Equals objInfo.ID_Object
                                   Join objValuta In objDBLevel_BTR_Valuta.ObjAtts On objBTR.GUID Equals objValuta.ID_Object
                                   Join objZahlAusg In objDBLevel_BTR_Zahlungsausgang.ObjAtts On objBTR.GUID Equals objZahlAusg.ID_Object
                                   Join objBegZahl In objDBLevel_BTR_BegZahl.ObjAtts On objBTR.GUID Equals objBegZahl.ID_Object
                                   Join objBetrag In objDBLevel_BTR_Betrag.ObjAtts On objBTR.GUID Equals objBetrag.ID_Object
                                   Join objBuchTxt In objDBLevel_BTR_Buchungstext.ObjAtts On objBTR.GUID Equals objBuchTxt.ID_Object
                                   Join objVerZweck In objDBLevel_BTR_Verwendungszweck.ObjAtts On objBTR.GUID Equals objVerZweck.ID_Object
                                   Join objAuftragskonto In objDBLevel_BTR_Auftragskonto.ObjectRels On objBTR.GUID Equals objAuftragskonto.ID_Object
                                   Where objAuftragskonto.Name_Other = objOItem_Konto_Mandant.Name
                                   Join objAuftragsbank In objDBLevel_BTR_BLZ.ObjectRels On objAuftragskonto.ID_Other Equals objAuftragsbank.ID_Object
                                   Join objGegenkonto In objDBLevel_BTR_Gegenkonto.ObjectRels On objBTR.GUID Equals objGegenkonto.ID_Object
                                   Join objGegenbank In objDBLevel_BTR_BLZ.ObjectRels On objGegenkonto.ID_Other Equals objGegenbank.ID_Object
                                   Join objCurrency In objDBLevel_BTR_Currencies.ObjectRels On objBTR.GUID Equals objCurrency.ID_Object
                                   Join objAltCur In objDBLevel_BTR_AltCurr.ObjectRels On objCurrency.ID_Other Equals objAltCur.ID_Object
                                   Group Join objPayment In objDBLevel_BTR_Payment.ObjectRels On objBTR.GUID Equals objPayment.ID_Object Into objPayments = Group
                                   From objPayment In objPayments.DefaultIfEmpty
                                   Order By objValuta.Val_Date Descending

            For Each objTransaction In objLTransactions
                If objTransaction.objPayment Is Nothing Then
                    dtblT_Banktransactions.Rows.Add(objTransaction.objBTR.GUID, _
                                                    objTransaction.objBTR.Name, _
                                                    objOItem_BankTransactionClass.GUID, _
                                                objTransaction.objBegZahl.ID_Attribute, _
                                                objTransaction.objBegZahl.Val_String, _
                                                objTransaction.objBuchTxt.ID_Attribute, _
                                                objTransaction.objBuchTxt.Val_String, _
                                                objTransaction.objInfo.ID_Attribute, _
                                                objTransaction.objInfo.Val_String, _
                                                objTransaction.objZahlAusg.ID_Attribute, _
                                                objTransaction.objZahlAusg.Val_Bit, _
                                                objTransaction.objAuftragskonto.ID_Other, _
                                                objTransaction.objAuftragskonto.Name_Other, _
                                                objTransaction.objAuftragsbank.ID_Other, _
                                                objTransaction.objAuftragsbank.Name_Other, _
                                                Nothing, _
                                                Nothing, _
                                                objTransaction.objGegenkonto.ID_Other, _
                                                objTransaction.objGegenkonto.Name_Other, _
                                                objTransaction.objGegenbank.ID_Other, _
                                                objTransaction.objGegenbank.Name_Other, _
                                                objTransaction.objCurrency.ID_Other, _
                                                objTransaction.objCurrency.Name_Other, _
                                                objTransaction.objAltCur.ID_Other, _
                                                objTransaction.objAltCur.Name_Other, _
                                                objTransaction.objValuta.ID_Attribute, _
                                                objTransaction.objValuta.Val_Date, _
                                                objTransaction.objVerZweck.ID_Attribute, _
                                                objTransaction.objVerZweck.Val_String, _
                                                objTransaction.objBetrag.ID_Attribute, _
                                                objTransaction.objBetrag.Val_Double)
                Else
                    dtblT_Banktransactions.Rows.Add(objTransaction.objBTR.GUID, _
                                                    objTransaction.objBTR.Name, _
                                                    objOItem_BankTransactionClass.GUID, _
                                                objTransaction.objBegZahl.ID_Attribute, _
                                                objTransaction.objBegZahl.Val_String, _
                                                objTransaction.objBuchTxt.ID_Attribute, _
                                                objTransaction.objBuchTxt.Val_String, _
                                                objTransaction.objInfo.ID_Attribute, _
                                                objTransaction.objInfo.Val_String, _
                                                objTransaction.objZahlAusg.ID_Attribute, _
                                                objTransaction.objZahlAusg.Val_Bit, _
                                                objTransaction.objAuftragskonto.ID_Other, _
                                                objTransaction.objAuftragskonto.Name_Other, _
                                                objTransaction.objAuftragsbank.ID_Other, _
                                                objTransaction.objAuftragsbank.Name_Other, _
                                                objTransaction.objPayment.ID_Other, _
                                                objTransaction.objPayment.Name_Other, _
                                                objTransaction.objGegenkonto.ID_Other, _
                                                objTransaction.objGegenkonto.Name_Other, _
                                                objTransaction.objGegenbank.ID_Other, _
                                                objTransaction.objGegenbank.Name_Other, _
                                                objTransaction.objCurrency.ID_Other, _
                                                objTransaction.objCurrency.Name_Other, _
                                                objTransaction.objAltCur.ID_Other, _
                                                objTransaction.objAltCur.Name_Other, _
                                                objTransaction.objValuta.ID_Attribute, _
                                                objTransaction.objValuta.Val_Date, _
                                                objTransaction.objVerZweck.ID_Attribute, _
                                                objTransaction.objVerZweck.Val_String, _
                                                objTransaction.objBetrag.ID_Attribute, _
                                                objTransaction.objBetrag.Val_Double)
                End If


            Next

            objOItem_Result_BankTransaction = objLocalConfig.Globals.LState_Success
        End If
    End Sub

    Public Sub get_Data_AltCurr()
        Dim objOL_AltCurr As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem

        objOL_AltCurr.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Currencies.GUID,
                                                 .ID_Parent_Other = objLocalConfig.OItem_Type_Alternate_Currency_Name.GUID,
                                                 .ID_RelationType = objLocalConfig.OItem_RelationType_offers.GUID})

        objOItem_Result = objDBLevel_BTR_AltCurr.GetDataObjectRel(objOL_AltCurr, _
                                                                    doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Auftragskonto()
        Dim objOL_Auftragskonto As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem

        objOL_Auftragskonto.Add(New clsObjectRel With {.ID_Parent_Object = objOItem_BankTransactionClass.GUID,
                                                       .ID_Parent_Other = objLocalConfig.OItem_Type_Kontonummer.GUID,
                                                       .ID_RelationType = objLocalConfig.OItem_RelationType_Auftragskonto.GUID})


        objOItem_Result = objDBLevel_BTR_Auftragskonto.GetDataObjectRel(objOL_Auftragskonto, _
                                                        doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub


    Public Sub get_Data_BegZahl()
        Dim objOL_BegZahl As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_BegZahl.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                 .ID_AttributeType = objLocalConfig.OItem_Attribute_Beg_nstigter_Zahlungspflichtiger.GUID})
            
        objOItem_Result = objDBLevel_BTR_BegZahl.GetDataObjectAtt(objOL_BegZahl, _
                                                                    doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Betrag()
        Dim objOL_Betrag As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Betrag.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                .ID_AttributeType = objLocalConfig.OItem_Attribute_Betrag.GUID})

        objOItem_Result = objDBLevel_BTR_Betrag.GetDataObjectAtt(objOL_Betrag, _
                                                                   doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If

    End Sub

    Public Sub get_Data_BLZ()
        Dim objOItem_Result As clsOntologyItem
        Dim objOL_KontoBank As New List(Of clsObjectRel)


        objOL_KontoBank.Add(New clsObjectRel With {.ID_Parent_Object = objLocalConfig.OItem_Type_Kontonummer.GUID,
                                                   .ID_Parent_Other = objLocalConfig.OItem_Type_Bankleitzahl.GUID,
                                                   .ID_RelationType = objLocalConfig.OItem_RelationType_belongsTo.GUID})

        objOItem_Result = objDBLevel_BTR_BLZ.GetDataObjectRel(objOL_KontoBank, _
                                                                      doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Buchungstext()
        Dim objOL_Buchungstext As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Buchungstext.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                      .ID_AttributeType = objLocalConfig.OItem_Attribute_Buchungstext.GUID})
            
        objOItem_Result = objDBLevel_BTR_Buchungstext.GetDataObjectAtt(objOL_Buchungstext, _
                                                                         doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If

    End Sub

    Public Sub get_Data_Currencies()
        Dim objOL_Currency As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem

        objOL_Currency.Add(New clsObjectRel With {.ID_Parent_Object = objOItem_BankTransactionClass.GUID,
                                                  .ID_Parent_Other = objLocalConfig.OItem_Type_Currencies.GUID,
                                                  .ID_RelationType = objLocalConfig.OItem_RelationType_belonging.GUID})

        objOItem_Result = objDBLevel_BTR_Currencies.GetDataObjectRel(objOL_Currency, _
                                                                       doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Gegenkonto()
        Dim objOL_Gegenkonto As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem

        objOL_Gegenkonto.Add(New clsObjectRel With {.ID_Parent_Object = objOItem_BankTransactionClass.GUID,
                                                    .ID_Parent_Other = objLocalConfig.OItem_Type_Kontonummer.GUID,
                                                    .ID_RelationType = objLocalConfig.OItem_RelationType_Gegenkonto.GUID})

        objOItem_Result = objDBLevel_BTR_Gegenkonto.GetDataObjectRel(objOL_Gegenkonto, _
                                                                           doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Payment()
        Dim objOL_Payment As New List(Of clsObjectRel)
        Dim objOItem_Result As clsOntologyItem

        objOL_Payment.Add(New clsObjectRel With {.ID_Parent_Object = objOItem_BankTransactionClass.GUID,
                                                 .ID_Parent_Other = objLocalConfig.OItem_Type_Payment.GUID,
                                                 .ID_RelationType = objLocalConfig.OItem_RelationType_belongsTo.GUID})

        objOItem_Result = objDBLevel_BTR_Payment.GetDataObjectRel(objOL_Payment, _
                                                                    doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If

    End Sub

    Public Sub get_Data_Valuta()
        Dim objOL_Valuta As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Valuta.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                .ID_AttributeType = objLocalConfig.OItem_Attribute_Valutatag.GUID})
            
        objOItem_Result = objDBLevel_BTR_Valuta.GetDataObjectAtt(objOL_Valuta, _
                                                                   doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Verwendungszweck()
        Dim objOL_Verwendungszweck As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Verwendungszweck.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                          .ID_AttributeType = objLocalConfig.OItem_Attribute_Verwendungszweck.GUID})
            
        objOItem_Result = objDBLevel_BTR_Verwendungszweck.GetDataObjectAtt(objOL_Verwendungszweck, _
                                                                             doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Zahlungsausgang()
        Dim objOL_Zahlungsausgang As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Zahlungsausgang.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                                         .ID_AttributeType = objLocalConfig.OItem_Attribute_Zahlungsausgang.GUID})

        objOItem_Result = objDBLevel_BTR_Zahlungsausgang.GetDataObjectAtt(objOL_Zahlungsausgang, _
                                                                            doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If
    End Sub

    Public Sub get_Data_Info()
        Dim objOL_Info As New List(Of clsObjectAtt)
        Dim objOItem_Result As clsOntologyItem

        objOL_Info.Add(New clsObjectAtt With {.ID_Class = objOItem_BankTransactionClass.GUID,
                                              .ID_AttributeType = objLocalConfig.OItem_Attribute_Info.GUID})
            
        objOItem_Result = objDBLevel_BTR_Info.GetDataObjectAtt(objOL_Info, _
                                                                 doIds:=False)

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
            objOItem_Result_BankTransaction = objOItem_Result
        End If



    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig, ByVal OItem_Partner As clsOntologyItem)
        objLocalConfig = LocalConfig

        objOItem_Partner = OItem_Partner

        set_DBConnection()
    End Sub

    Private Sub set_DBConnection()
        objDBLevel_ImportSettings_Att_ColHeader = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Att_Start = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_Files = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_TextQualifier = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_TextSeperator = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_ImportLog = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_IS_To_BankClass = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_Partner = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_ImportSettings_Rel_Bank = New OntologyModDBConnector(objLocalConfig.Globals)

        objDBLevel_BTR = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Auftragskonto = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_BLZ = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Gegenkonto = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_AltCurr = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_BegZahl = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Betrag = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Buchungstext = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Currencies = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Info = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Payment = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Valuta = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Verwendungszweck = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_BTR_Zahlungsausgang = New OntologyModDBConnector(objLocalConfig.Globals)

        objDBLevel_Kontodaten = New OntologyModDBConnector(objLocalConfig.Globals)

        objFileWork = New clsFileWork(objLocalConfig.Globals)
    End Sub
End Class




